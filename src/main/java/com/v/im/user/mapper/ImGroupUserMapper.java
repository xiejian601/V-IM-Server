package com.v.im.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.v.im.user.entity.ImGroupUser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author v
 * @since 2018-10-23
 */
public interface ImGroupUserMapper extends BaseMapper<ImGroupUser> {

}

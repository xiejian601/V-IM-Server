package com.v.im.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.v.im.user.entity.ImGroup;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author v
 * @since 2018-10-23
 */
public interface IImGroupService extends IService<ImGroup> {

}

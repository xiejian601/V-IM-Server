package com.v.im.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.v.im.user.entity.ImChatGroup;

/**
 * <p>
 * 群 服务类
 * </p>
 *
 * @author v
 * @since 2018-10-28
 */
public interface IImChatGroupService extends IService<ImChatGroup> {

}

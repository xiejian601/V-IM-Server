package com.v.im.user.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author v
 * @since 2018-10-23
 */
@RestController
@RequestMapping("/user/im-group")
public class ImGroupController {

}

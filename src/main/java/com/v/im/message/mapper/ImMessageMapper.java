package com.v.im.message.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.v.im.message.entity.ImMessage;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author v
 * @since 2018-10-08
 */
public interface ImMessageMapper extends BaseMapper<ImMessage> {

}
